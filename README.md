# INTRODUCCION REACT JS

[React wiki midudev](https://www.reactjs.wiki/)

## ¿Qué es un componente?

- Componente: Un componente es una porción de código independiente y reutilizable que permite modularizar y organizar el desarrollo de aplicaciones. Los componentes encapsulan tanto la estructura visual como la lógica relacionada en un mismo bloque, lo que facilita su mantenimiento, reutilización y escalabilidad en proyectos web y de software.
- Elemento:

## ¿Cómo se comunican los componentes? Props y atributos

- `Props` son los parametros que resiven los componentes, internamente son un objeto

# Eventos en React: onClick, onChange

- Los eventos nos ayudan a darle mas iteractividad al la palicacion eventos como `onClick y onchange`, en react todo lo que inicia con `on` es un evento es un `addEventListener` de js, su estructura se basa en que dentro del cuerpo se debe de llamar siempre a una funcion
- onclick

# Emitiendo Eventos entre componentes

En React, la comunicación entre componentes se realiza principalmente a través de la propagación de datos desde los componentes padres a los componentes hijos utilizando props. Sin embargo, para comunicar eventos desde un componente hijo a un componente padre, se utiliza un patrón conocido como "levantamiento de estado" (lifting state up).

Aquí hay un ejemplo de cómo puedes emitir eventos desde un componente hijo a un componente padre en React:

1. **Componente Padre (`ParentComponent.js`):**

```jsx
import React, { useState } from "react";
import ChildComponent from "./ChildComponent";

function ParentComponent() {
  const [message, setMessage] = useState("");

  const handleChildEvent = (eventData) => {
    setMessage(eventData);
  };

  return (
    <div>
      <h1>Componente Padre</h1>
      <p>Mensaje del componente hijo: {message}</p>
      <ChildComponent onChildEvent={handleChildEvent} />
    </div>
  );
}

export default ParentComponent;
```

2. **Componente Hijo (`ChildComponent.js`):**

```jsx
import React from "react";

function ChildComponent(props) {
  const handleClick = () => {
    const eventData = "¡Hola desde el componente hijo!";
    props.onChildEvent(eventData);
  };

  return (
    <div>
      <h2>Componente Hijo</h2>
      <button onClick={handleClick}>Enviar Mensaje al Padre</button>
    </div>
  );
}

export default ChildComponent;
```

En este ejemplo, el componente hijo `ChildComponent` recibe una prop llamada `onChildEvent`, que es una función pasada desde el componente padre. Cuando se hace clic en el botón en el componente hijo, se llama a la función `handleClick`, que emite un evento al componente padre a través de la función `onChildEvent`, pasando un mensaje como datos del evento.

El componente padre, `ParentComponent`, define una función `handleChildEvent` que se utiliza para actualizar el estado con el mensaje recibido del componente hijo. Luego, el mensaje actualizado se muestra en el componente padre.

Este patrón permite que el componente hijo emita eventos y notifique al componente padre sobre ciertas acciones. Es importante destacar que el componente padre controla el estado y la lógica relacionada con los eventos, mientras que el componente hijo simplemente llama a la función proporcionada por el padre para notificar eventos.

# Estados

## ¿Qué es el estado?: El estado o los estados

En React, el estado es un objeto que contiene datos que pueden cambiar en el tiempo. Se utiliza para controlar los cambios en la interfaz de usuario (IU). Por ejemplo, el estado podría usarse para almacenar la cantidad de elementos en una lista o el estado de un formulario.

El estado se declara en un componente de React usando la función `useState`. Esta función devuelve un par de valores, el estado actual y una función que se puede usar para actualizar el estado.

Para actualizar el estado, se llama a la función de actualización con los nuevos valores de estado. La actualización del estado se realiza de forma asíncrona, lo que significa que los cambios no se reflejan en la interfaz de usuario de inmediato.

El estado se utiliza en React para proporcionar una forma de manejar los datos que cambian con el tiempo. Esto permite a los componentes reaccionar a los cambios en los datos y actualizar la IU en consecuencia.

Aquí hay un ejemplo de cómo usar el estado en React:

```javascript
function Counter() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <h1>Contador: {count}</h1>
      <button onClick={() => setCount(count + 1)}>+1</button>
    </div>
  );
}
```

En este ejemplo, el estado contiene un número que representa el número de clics en el botón. Cada vez que se hace clic en el botón, la función `setCount()` se llama con un nuevo valor para el número de clics. La actualización del estado se realiza de forma asíncrona, por lo que el número de clics no se actualiza inmediatamente.

- porque esto es un array '[count, setCount]' esto se debe a que para cambiar el estado se debe crear una funcion eto se debe a que el estado es inmutable, no se le le puede cambiar el valor con solo `estado = nuevoestado`, esto esta mal entonces por eso se crea un funcion que nos permita actualizar el estado que es coontrolada por react, `[variableDelEstado, funcionDelEstado]` por convencion la funcion debe iniciar con `set`
  El estado se puede utilizar para una variedad de propósitos en React. Es una herramienta poderosa que puede ayudar a los desarrolladores a crear interfaces de usuario complejas y receptivas.

# Como compartir estados entre componentes

# Local Storage con React.js

Evita acceder al localStorage dentro del componente
Acceder a los valores del localStorage dentro del componente es muy pesado en cuanto al rendimiento, ya que se ejecuta sincrónicamente en cada re-renderizado del componente. En su lugar, puedes leerlo utilizando un callback que retorne el valor inicial del useState, esto permitirá acceder a la información una sola vez al momento que se crea el componente, esto por la definición de useState.

- El localstorage no puiede almacenar estructuras complejas, solo alamacena strings

# Custom Hooks

- Nos permiten abtraer la logica de nuestro componente para su posterior reutilizacion, se les conoce como cutom hooks a los hooks que vayas creando
- cuando implementarlo

## Organizacion de archivos y carpetas

## ¿Qué son los efectos en React?
- los efectos de react nos ayudan a sincronizar componnetes con sistemas externos. 

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
