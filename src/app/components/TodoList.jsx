

function TodoList(props) {
    //console.log(props.children)
    return (
        <div className={`grid gap-3 min-h-[50%] h-[50%] p-2 mb-3 borde rounded-2xl scroll-animation glass-bg border-collapse border-4 border-sky-500 overflow-y-scroll `}>
            {props.children}
        </div>
    )
}

export default TodoList