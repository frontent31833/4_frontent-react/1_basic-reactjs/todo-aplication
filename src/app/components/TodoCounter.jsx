

function TodoCounter({total, completed}) {
  return (
    <div className="text-center my-2">
      <h1 className="text-4xl font-bold text-sky-500 dark:text-sky-400 my-1">Todo List</h1>
      <p className="text-base font-bold text-slate-700 dark:text-slate-500 my-1">Has completado {completed} de {total} Todos</p>
    </div>
  )
}

export default TodoCounter