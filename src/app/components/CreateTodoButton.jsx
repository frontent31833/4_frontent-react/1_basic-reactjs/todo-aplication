

function CreateTodoButton() {
  
  const handleClick = (e) => {
    console.log("click", e);
  }

  return (
    <div className="flex justify-center">
      <button onClick={handleClick} type="button" className="bg-blue-600 px-4 py-2 rounded-xl">
        <svg className="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 18">
          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 1v16M1 9h16" />
        </svg>
        <span className="sr-only">Icon description</span>
      </button>
    </div>
  )
}

export default CreateTodoButton