import React from 'react'

function NavBar() {
    return (
        <nav className="border-gray-200">
            <ul className='grid h-[100vh] grid-rows-9 gap-5 justify-center items-center'>
                <li className='flex row-start-1 row-end-3 justify-center items-center'>
                    <a className='flex flex-col justify-center items-center' href="https://flowbite.com" >
                        <img className='flex justify-center items-center h-8 mr-3' src="https://flowbite.com/docs/images/logo.svg" alt="Flowbite Logo" />
                    </a>
                </li>
                <li className='flex justify-center items-center'>
                    <svg className="w-[30px] h-[30px] text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" width="20" height="20"><path d="m19.707 9.293-2-2-7-7a1 1 0 0 0-1.414 0l-7 7-2 2a1 1 0 0 0 1.414 1.414L2 10.414V18a2 2 0 0 0 2 2h3a1 1 0 0 0 1-1v-4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v4a1 1 0 0 0 1 1h3a2 2 0 0 0 2-2v-7.586l.293.293a1 1 0 0 0 1.414-1.414Z" /></svg>
                </li>
                <li className='flex justify-center items-center'>
                    <svg className="w-[30px] h-[30px] text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 18" width="20" height="18"><path d="M18 0H2a2 2 0 0 0 -2 2v10a2 2 0 0 0 2 2h3.546l3.2 3.659a1 1 0 0 0 1.506 0L13.454 14H18a2 2 0 0 0 2 -2V2a2 2 0 0 0 -2 -2Zm-8 10H5a1 1 0 0 1 0 -2h5a1 1 0 1 1 0 2Zm5 -4H5a1 1 0 0 1 0 -2h10a1 1 0 1 1 0 2Z" /></svg>

                </li>
                <li className='flex justify-center items-center'>
                    <svg className="w-[30px] h-[30px] text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 14 18" width="14" height="18"><path d="M7 9a4.5 4.5 0 1 0 0 -9 4.5 4.5 0 0 0 0 9Zm2 1H5a5.006 5.006 0 0 0 -5 5v2a1 1 0 0 0 1 1h12a1 1 0 0 0 1 -1v-2a5.006 5.006 0 0 0 -5 -5Z" /></svg>
                </li>
                <li className='flex justify-center items-center'>
                    <svg className="w-[30px] h-[30px] text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 19" width="20" height="19"><path d="M7.324 9.917A2.479 2.479 0 0 1 7.99 7.7l0.71 -0.71a2.484 2.484 0 0 1 2.222 -0.688 4.538 4.538 0 1 0 -3.6 3.615h0.002ZM7.99 18.3a2.5 2.5 0 0 1 -0.6 -2.564A2.5 2.5 0 0 1 6 13.5v-1c0.005 -0.544 0.19 -1.072 0.526 -1.5H5a5.006 5.006 0 0 0 -5 5v2a1 1 0 0 0 1 1h7.687l-0.697 -0.7ZM19.5 12h-1.12a4.441 4.441 0 0 0 -0.579 -1.387l0.8 -0.795a0.5 0.5 0 0 0 0 -0.707l-0.707 -0.707a0.5 0.5 0 0 0 -0.707 0l-0.795 0.8A4.443 4.443 0 0 0 15 8.62V7.5a0.5 0.5 0 0 0 -0.5 -0.5h-1a0.5 0.5 0 0 0 -0.5 0.5v1.12c-0.492 0.113 -0.96 0.309 -1.387 0.579l-0.795 -0.795a0.5 0.5 0 0 0 -0.707 0l-0.707 0.707a0.5 0.5 0 0 0 0 0.707l0.8 0.8c-0.272 0.424 -0.47 0.891 -0.584 1.382H8.5a0.5 0.5 0 0 0 -0.5 0.5v1a0.5 0.5 0 0 0 0.5 0.5h1.12c0.113 0.492 0.309 0.96 0.579 1.387l-0.795 0.795a0.5 0.5 0 0 0 0 0.707l0.707 0.707a0.5 0.5 0 0 0 0.707 0l0.8 -0.8c0.424 0.272 0.892 0.47 1.382 0.584v1.12a0.5 0.5 0 0 0 0.5 0.5h1a0.5 0.5 0 0 0 0.5 -0.5v-1.12c0.492 -0.113 0.96 -0.309 1.387 -0.579l0.795 0.8a0.5 0.5 0 0 0 0.707 0l0.707 -0.707a0.5 0.5 0 0 0 0 -0.707l-0.8 -0.795c0.273 -0.427 0.47 -0.898 0.584 -1.392h1.12a0.5 0.5 0 0 0 0.5 -0.5v-1a0.5 0.5 0 0 0 -0.5 -0.5ZM14 15.5a2.5 2.5 0 1 1 0 -5 2.5 2.5 0 0 1 0 5Z" /></svg>
                </li>
            </ul>
        </nav>

    )
}

export default NavBar