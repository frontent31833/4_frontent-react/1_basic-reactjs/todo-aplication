import { useState } from "react";
function useLocalstorage(key, initialValue) {
  // * localStorage manipulation
  const localStorageItem = localStorage.getItem(key);

  let parsetItem;
  if (!localStorageItem) {
    localStorage.setItem(key, JSON.stringify(initialValue));
    parsetItem = initialValue;
  } else {
    parsetItem = JSON.parse(localStorageItem);
  }

  const [item, setItem] = useState(parsetItem);

  const saveItem = (value) => {
    localStorage.setItem(key, JSON.stringify(value));
    setItem(value);
  };

  return [item, saveItem];
}

export { useLocalstorage };
