"use client"
import NavBar from '../components/NavBar'
import TodoCounter from "../components/TodoCounter";
import TodoSearch from "../components/TodoSearch";
import TodoList from "../components/TodoList";
import TodoItem from "../components/TodoItem";
import CreateTodoButton from "../components/CreateTodoButton";
import { useState, useEffect } from 'react'

import { CreateIcon, ListIcon, LogoutIcon } from '../icons/DashboardIcons'


// datas utils
import { mapDay, mapMonth, dataTodos } from '../Utils/DataTodos.js'

//Local storage
import { useLocalstorage } from '@/app/hooks/useLocalstorage.js'

function Dashboard() {
    
    const [dataclock, setDataClock] = useState(new Date());
    // * todo list
    const [todos, setDataTodos] = useLocalstorage('TodosData',  [])

    // * search
    const [searchValue, setSearchValue] = useState('')

    // * show todos oncompleted
    const amountTodosCompleted = todos.filter(todo => !!todo.completed).length
    // * show all todos
    const amountTodos = todos.length

    const searchetTodos = todos.filter((todo) => {
        return todo.text.toLowerCase().includes(searchValue.toLowerCase())
    })


    // * set completed todos
    function completedTodo(text) {
        const newTodos = [...todos]
        const todoIndex = newTodos.findIndex((item) => {
            return item.text === text
        })
        newTodos[todoIndex].completed = true;
        saveTodos(newTodos)
    }

    // * set oncompleted todos
    function offCompletedTodo(text) {
        const newTodos = [...todos]
        const todoIndex = newTodos.findIndex((item) => {
            return item.text === text
        })
        newTodos[todoIndex].completed = false;
        saveTodos(newTodos)
    }


    // * delete todos
    function deleteTodo(text) {
        const newTodos = [...todos]
        const todoIndex = newTodos.findIndex((item) => {
            return item.text === text
        })
        newTodos.splice(todoIndex, 1)
        saveTodos(newTodos)
    }


    useEffect(() => {
        const interval = setInterval(() => {
            setDataClock(new Date())
        }, 1000)
        return () => clearInterval(interval)
    }, [])

    return (
        <>
            <div className='grid grid-cols-16 gap-5 w-[99%] h-[99vh] m-auto'>
                <div className='h-full ol-span-1 col-end-auto rounded-xl overflow-hidden bg-glass '>
                    <NavBar />
                </div>
                <div className='h-full col-start-2 col-end-17 grid grid-cols-16 rounded-xl overflow-hidden bg-glass px-14 py-10 '>
                    {/* <NavBar /> */}
                    <div className='grid grid-rows-3 col-start-1 col-end-10'>
                        <div className='flex flex-col justify-start items-start '>
                            <p className='text-7xl font-bold text-sky-500 dark:text-sky-400'>
                                {dataclock.getHours()}:{(dataclock.getMinutes() < 10) ? `0${dataclock.getMinutes()}` : dataclock.getMinutes()}
                            </p>
                            <p className='text-xl font-bold text-slate-700 dark:text-slate-500'>
                                {mapDay.get(dataclock.getDay())}, {mapMonth.get(dataclock.getMonth())} {(dataclock.getMonth() < 10) ? `0${dataclock.getMonth()}` : dataclock.getMonth()}
                            </p>
                        </div>
                        <div className='grid grid-cols-3 w-[50%]'>
                            <button className='flex flex-col justify-start items-center '>
                                <div className='bg-blue-600 p-5 rounded-xl mb-5'>
                                    <CreateIcon />
                                </div>
                                <p className='font-bold text-xl text-center text-slate-700 dark:text-slate-500'>Todo</p>
                            </button>
                            <button className='flex flex-col justify-start items-center '>
                                <div className='bg-blue-600 p-5 rounded-xl mb-5'>
                                    <ListIcon />
                                </div>
                                <p className='font-bold text-xl text-center text-slate-700 dark:text-slate-500'>List</p>
                            </button>
                            <button className='flex flex-col justify-start items-center '>
                                <div className='bg-red-500 p-5 rounded-xl mb-5'>
                                    <LogoutIcon />
                                </div>
                                <p className='font-bold text-xl text-center text-slate-700 dark:text-slate-500'>Logout</p>
                            </button>
                        </div>

                        <div className='flex flex-col justify-start items-start'>
                            <h2 className='text-4xl pb-2 font-bold text-sky-500 dark:text-sky-400'>
                                Hi Alex...!
                            </h2>
                            <p className='text-slate-700 dark:text-slate-500 text-xl'>
                                Estas listo para regitrar todos y cumplirlos
                            </p>
                        </div>
                    </div>

                    <div className='col-start-10 col-end-17'>
                        <TodoCounter completed={amountTodosCompleted} total={amountTodos} />
                        <TodoSearch searchValue={searchValue} setSearchValue={setSearchValue} />
                        <TodoList >
                            {
                                searchetTodos.map((todo) => (
                                    <TodoItem
                                        text={todo.text}
                                        key={todo.text}
                                        completed={todo.completed}
                                        onComplete={() => completedTodo(todo.text)}
                                        offComplete={() => offCompletedTodo(todo.text)}
                                        onDelete={() => deleteTodo(todo.text)}
                                    />
                                ))
                            }
                        </TodoList>

                        <CreateTodoButton />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Dashboard