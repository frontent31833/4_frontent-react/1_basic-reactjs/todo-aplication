"use client"

import { useState, useEffect } from "react";
import Auth from "./auth";
import Dashboard from "./Dashboard";
import HomePage from "./HomePage";
import Modal from "../components/Modal";

function App() {
    const [auth, setAuth] = useState(false);
    const [showHome, setShowHome] = useState(false);


    const handleData = (eventData) => {
        console.log(eventData);
        if (
            eventData.password === "12345678" &&
            eventData.userName === "alguien@gmail.com"
        ) {
            setAuth(true);
        }
    };

    useEffect(() => {
        window.setTimeout(() => {
            setShowHome(true);
        }, 7000);
    }, []);
    return (
        <div>{!showHome ? (
            <HomePage />
        ) : (
            <main>
                {!auth ? (
                    <div className="container mx-auto">
                        <Auth onChildEvent={handleData} />
                    </div>
                ) : (
                    <div className="w-[99%] mx-auto">
                        <Dashboard />
                    </div>
                )}
            </main>
        )}</div>
    )
}

export default App