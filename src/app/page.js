"use client";

import App from "./layout/App";

export default function Home() {
  
  return (
    <>
      <App />
    </>
  );
}
